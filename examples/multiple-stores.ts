import { TreeNode } from '../src/tree-node';
import { TreeBranch } from '../src/tree-branch';
import { Subscriber } from '../src/subscriber';

const cities = [
  'Paris',
  'Tokyo',
];

const weatherData = [
  ['New York', 20, 'Celsius'],
  ['Paris', 10, 'Celsius'],
  ['Tokyo', 25, 'Celsius'],
  ['Moscow', 5, 'Celsius'],
];

// usually you set up stores in their own class files
class CityStore extends TreeNode {
  private data = [];

  constructor() {
    super();
  }

  // this abstract method needs to be implemented,
  // and is responsible for refreshing store data
  // it is also good idea to notify subscribers right 
  // when data has been refreshed
  refresh() {
    this.data = cities;
    this.publish();
  }

  getData(): string[] {
    return this.data;
  }
}


class WeatherStore extends TreeNode {
  private data = [];

  constructor() {
    super();
  }

  refresh() {
    this.data = weatherData;
    this.publish();
  }

  getData(): [string, number, string][] {
    return this.data;
  }
}

const cityStore = new CityStore();
const weatherStore = new WeatherStore();
// construct storage tree with root node and child nodes in branches
const tree = new TreeBranch(cityStore, [new TreeBranch(weatherStore, [])]);

// subscribers are notified whenever store data changes,
// and execute the given callback function, of type '() => void'
weatherStore.addSubscriber(new Subscriber('city-forecast', () => {
  console.log('Weather forecast for selected cities');
  console.log('------------------------------------');

  cityStore.getData().forEach(city => {
    const forecast = weatherData.find(w => w[0] === city);
    forecast === undefined ?
      console.log('could not find weather forecast for city ' + city) :
      console.log(forecast[0] + ': ' + forecast[1] + ' ' + forecast[2]);
  });
}))

// refresh all stores in storage tree with one call
tree.refresh();
