import { TreeNode } from '../src/tree-node';
import { Subscriber } from '../src/subscriber';

const urls = [
  'https://www.google.com',
  'https://www.facebook.com',
  'https://www.apple.com',
];

// set up stores in their own class files
class MasterStore extends TreeNode {
  data = [];

  constructor() {
    super();
  }

  // this abstract method needs to be implemented,
  // and is responsible for refreshing store data
  refresh() {
    this.data = urls;
  }

  // call publish method after data is refreshed to notify subscribers
  fetchData() {
    this.refresh();
    this.publish();
  }
}

const store = new MasterStore();

// subscribers are notified whenever store data changes,
// and execute the given callback function, of type '() => void'
store.addSubscriber(new Subscriber('unique-subscriber-name', () => {
  store.data.forEach(url => console.log(url));
}))

// prints urls to console
store.fetchData();

store.removeSubscriber('unique-subscriber-name');

// prints nothing
store.fetchData();