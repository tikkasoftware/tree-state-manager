# Tree State Manager

This is a framework agnostic state management library, written in Typescript. Data is stored in stores, which are named tree nodes. Reference to trees comes from the fact that you can set up stores in a tree-like hierarchical structure (but don't have to). A tree-like store hierarchy enables stores to be handled in aggregate manner, for example refreshing all stores at once, or stores beneath a one branch.

## Installation

```
npm install tree-state-manager --save
```

## Usage

First create a class which extends TreeNode class. All TreeNodes must implement refresh() method, which must be responsible for updating/refreshing the internal state of the node class.

The base TreeNode class extends Notifier, which allows to subscribe to TreeNode storage state changes by adding a Subscriber instance to TreeNode listeners array.

A storage tree consists of TreeBranch-classes, which in turn wrap a TreeNode instance, plus an array of child branches, each in turn wrapping their own TreeNode. This hierarchy allows to update/clear the whole data storage tree created with single call. This is especially handy, when you need to update lots of data storages at once.

See [examples/](https://gitlab.com/tikkasoftware/tree-state-manager/-/tree/master/examples) folder for usage examples.

## Development

To install development dependencies:
`npm i`

Run example files:
`sh run-example.sh examples/one-store` or `sh run-example.sh examples/multiple-stores`.

### Tests

`npm test` or `npm test <spec file>`.
