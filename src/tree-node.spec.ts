import { TreeNode } from './tree-node';

class TreeNodeImplementation extends TreeNode {
  refresh() {}
}
const node = new TreeNodeImplementation();

describe('TreeNode', () => {
  it('should create an instance', () => {
    expect(node).toBeTruthy();
  });
});
