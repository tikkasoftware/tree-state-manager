import 'jasmine';

import { Notifier } from './notifier';
import { Subscriber } from './subscriber';

describe('Notifier', () => {
  class NotI extends Notifier {
    trigger() {
      this.publish();
    }
  }

  it('should create an instance', () => {
    expect(new Notifier()).toBeTruthy();
  });

  it('should notify subscriber', () => {
    let counter = 0;

    const not = new NotI();
    const sub = new Subscriber('joo', () => counter = counter + 1);

    not.addSubscriber(sub);
    not.trigger();

    expect(counter).toEqual(1);
  });

  it('should remove subscriber', () => {
    let counter = 0;

    const not = new NotI();
    const sub = new Subscriber('joo', () => counter = counter + 1);

    not.addSubscriber(sub);
    not.removeSubscriber(sub.name);
    not.trigger();

    expect(counter).toEqual(0);
  });
});
