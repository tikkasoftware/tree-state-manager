import { TreeNode } from './tree-node';

export class TreeBranch {
  root: TreeNode;
  childBranches: TreeBranch[];

  constructor(
    root: TreeNode,
    childBranches: TreeBranch[]
  ) {
    this.root = root;
    this.childBranches = childBranches;
  }

  refresh() {
    this.refreshThisBranch();
  }

  private refreshThisBranch() {
    this.root.refresh();
    this.childBranches.forEach((child) => child.refreshThisBranch());
  }
}
