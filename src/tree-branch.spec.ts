import { TreeBranch } from './tree-branch';
import { TreeNode } from './tree-node';

class TreeNodeImplementation extends TreeNode {
  refresh() {}
}
const node = new TreeNodeImplementation();

describe('TreeBranch', () => {
  it('should create an instance', () => {
    expect(new TreeBranch(node, [])).toBeTruthy();
  });
});
