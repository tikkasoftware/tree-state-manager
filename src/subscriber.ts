export class Subscriber {
  public name: string;
  public callback: () => void;

  public notify() {
    this.callback();
  }

  constructor(name: string, callback: () => void) {
    this.name = name;
    this.callback = callback;
  }
}
