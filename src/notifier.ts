import { Subscriber } from './subscriber';

export class Notifier {
  protected subscribers: Subscriber[] = [];

  addSubscriber(sub: Subscriber) {
    this.subscribers.push(sub);
  }

  removeSubscriber(name: string) {
    this.subscribers = this.subscribers.filter(sub => {
      return sub.name !== name;
    });
  }

  protected publish() {
    this.subscribers.forEach((sub) => {
      sub.notify();
    });
  }
}
