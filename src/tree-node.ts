import { Notifier } from './notifier';

export abstract class TreeNode extends Notifier {
  constructor() {
    super();
  }

  abstract refresh(...args: any[]): void;
}
